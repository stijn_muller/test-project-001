/PROG  BG_SIGNALS	  Macro
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "";
PROG_SIZE	= 1127;
CREATE		= DATE 19-04-12  TIME 12:56:56;
MODIFIED	= DATE 21-01-07  TIME 14:24:30;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 43;
MEMORY_SIZE	= 1455;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= *,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : FALSE;
/MN
   1:  !UOP Signals ;
   2:  !Systeem Signalen Bedienpult ;
   3:   ;
   4:  !Set Auto flag ;
   5:  F[40]=(!UO[8:TP enabled] AND DO[217]) ;
   6:   ;
   7:  !IMDIATE STOP ;
   8:  F[1:*IMSTP]=(ON) ;
   9:  !SAFETY SPEED ;
  10:  F[3:*SFSPD]=(ON) ;
  11:  F[8:Enable]=(ON) ;
  12:   ;
  13:  !Start condities  Groene knop ;
  14:  F[6:Start]=(F[40] AND DI[6]) ;
  15:   ;
  16:  !HOLD UI[2]  ;
  17:  !Is rechtstreeks gemapped  ;
  18:  !op DI[2:*Hold] Rack 1  1  2 ;
  19:   ;
  20:  !Reset condities  Blauwe knop ;
  21:  F[5:Fault reset]=(F[40] AND DI[5]) ;
  22:   ;
  23:  !Home condities  Gele knop ;
  24:  F[7:Home]=(F[40] AND DI[7] AND !UO[3:Prg running] AND !UO[4:Prg paused]) ;
  25:   ;
  26:   ;
  27:  !Lamp condities ;
  28:  !-------------------------------- ;
  29:   ;
  30:  !Start Lamp condities  GROEN ;
  31:  DO[3]=(F[40] AND (UO[3:Prg running] OR (UO[4:Prg paused] AND !UO[6:Fault] AND F[1011]) OR (UO[1:Cmd enabled] AND !UO[3:Prg running] AND F[1011]))) ;
  32:   ;
  33:  !Fout Lamp condities  ROOD ;
  34:  DO[6]=(F[40] AND UO[6:Fault]) ;
  35:   ;
  36:  !Reset Lamp condities  BLAUW ;
  37:  DO[8]=(F[40] AND ((UO[4:Prg paused] AND F[1013]) OR (UO[6:Fault] AND !UO[4:Prg paused] AND F[1011]))) ;
  38:   ;
  39:  !Home Lamp condities  GEEL ;
  40:  DO[7]=(F[40] AND UO[7:At perch]) ;
  41:   ;
  42:   ;
  43:   ;
/POS
/END
