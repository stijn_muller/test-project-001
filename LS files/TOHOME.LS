/PROG  TOHOME
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "";
PROG_SIZE	= 496;
CREATE		= DATE 21-01-07  TIME 19:16:30;
MODIFIED	= DATE 21-01-07  TIME 19:16:30;
FILE_NAME	= TO_HOME;
VERSION		= 0;
LINE_COUNT	= 18;
MEMORY_SIZE	= 796;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= 1,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL
/MN
   1:  !******************************   ;
   2:  !AKAB   ;
   3:  !De Koumen 58   ;
   4:  !6433 KD  Hoensbroek   ;
   5:  ! ;
   6:  !In samenwerking met   ;
   7:  !Affix Engineering   ;
   8:  !******************************** ;
   9:   ;
  10:  UFRAME_NUM=0 ;
  11:  UTOOL_NUM=1 ;
  12:  PAYLOAD[1] ;
  13:   ;
  14:   ;
  15:J PR[1:Home positie] 20% CNT100    ;
  16:   ;
  17:   ;
  18:   ;
/POS
/END
