/PROG  BG_CHECK	  Macro
/ATTR
OWNER		= MNEDITOR;
COMMENT		= "";
PROG_SIZE	= 779;
CREATE		= DATE 19-04-09  TIME 13:09:22;
MODIFIED	= DATE 19-04-09  TIME 13:31:46;
FILE_NAME	= ;
VERSION		= 0;
LINE_COUNT	= 36;
MEMORY_SIZE	= 1267;
PROTECT		= READ_WRITE;
TCD:  STACK_SIZE	= 0,
      TASK_PRIORITY	= 50,
      TIME_SLICE	= 0,
      BUSY_LAMP_OFF	= 0,
      ABORT_REQUEST	= 0,
      PAUSE_REQUEST	= 0;
DEFAULT_GROUP	= *,*,*,*,*;
CONTROL_CODE	= 00000000 00000000;
/APPL

AUTO_SINGULARITY_HEADER;
  ENABLE_SINGULARITY_AVOIDANCE   : FALSE;
/MN
   1:  !Check BG LOGIC ;
   2:   ;
   3:  LBL[1:BG Logic check] ;
   4:  IF ($MIX_BG[1].$STATUS<=1),JMP LBL[101] ;
   5:  IF ($MIX_BG[2].$STATUS<=1),JMP LBL[102] ;
   6:  IF ($MIX_BG[3].$STATUS<=1),JMP LBL[103] ;
   7:   ;
   8:  END ;
   9:   ;
  10:  LBL[101] ;
  11:  macro_119    ;
  12:  MESSAGE[BG_LOGIC 1 Niet aktief] ;
  13:  MESSAGE[BG LOGIC 1 N.O.K] ;
  14:  UALM[12] ;
  15:  JMP LBL[1] ;
  16:   ;
  17:  LBL[102] ;
  18:  macro_119    ;
  19:  MESSAGE[BG_LOGIC 2 Niet aktief] ;
  20:  MESSAGE[BG LOGIC 2 N.O.K] ;
  21:  UALM[12] ;
  22:  JMP LBL[1] ;
  23:   ;
  24:  LBL[103] ;
  25:  macro_119    ;
  26:  MESSAGE[BG_LOGIC 3 Niet aktief] ;
  27:  MESSAGE[BG LOGIC 3 N.O.K] ;
  28:  UALM[12] ;
  29:  JMP LBL[1] ;
  30:   ;
  31:   ;
  32:   ;
  33:   ;
  34:   ;
  35:   ;
  36:   ;
/POS
/END
